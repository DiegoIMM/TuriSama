package com.example.salatiel.tesis.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class InicioSQLiteOpenHelper extends SQLiteOpenHelper {

    public InicioSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table usuario(tokensql text primary key, nombresql text, estadosql int, idsql int, categoriasql text)");
        db.execSQL("create table sector(id_sector int primary key, nombre_sector text, descripcion_sector text)");
        db.execSQL("create table datosruta(id_ruta integer primary key autoincrement, nombre_ruta text, id_sector int, descripcion_ruta text, distancia_final float, tiempo_estandar double, estado int)");
        db.execSQL("create table coordenadasruta(id_coordenadas integer primary key autoincrement, latitud text, longitud text, id_ruta int, posicion int, estado_coordenada int)");
        db.execSQL("create table record(id_record integer primary key, id_ruta int, id_usuario int, tiempo double)");
     }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("create table usuario(tokensql text primary key, nombresql text, estadosql int, idsql int)");
    }
}
