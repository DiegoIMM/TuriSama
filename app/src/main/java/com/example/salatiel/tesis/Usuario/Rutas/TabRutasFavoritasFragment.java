package com.example.salatiel.tesis.Usuario.Rutas;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.salatiel.tesis.R;

public class TabRutasFavoritasFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_rutas_favoritas, container, false);

        if (Build.VERSION.SDK_INT >= 10){
            StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(policy);
        }
        /*try {

        }*/

        return view;
    }
}
