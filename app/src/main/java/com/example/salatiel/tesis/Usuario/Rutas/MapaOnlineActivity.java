package com.example.salatiel.tesis.Usuario.Rutas;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.NavegacionUsuarioActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MapaOnlineActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView tv01, tv02, tv03;
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post;
    private HttpResponse response;
    private HttpEntity entity;
    private List<NameValuePair> lista;
    private String responseString;
    private ArrayList<ContentValues> coordenadas = new ArrayList<ContentValues>();
    private ArrayList<ContentValues> estado = new ArrayList<ContentValues>();
    private Double latitud = null;
    private Double longitud = null;
    private MarkerOptions viejoMarcador = null;
    private Marker ultimoMarcador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_online);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        class EnviarDatos extends AsyncTask<String, String, String> {
            private Activity contexto;
            EnviarDatos(Activity context){
                this.contexto = context;
            }
            @Override
            protected String doInBackground(String... params) {
                if (AgregarFavorito()){
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Ruta agregada a favoritos", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Ruta no agregada a favoritos", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return null;
            }
        }

        tv01 = (TextView) findViewById(R.id.tvmapaonline01);
        tv02 = (TextView) findViewById(R.id.tvmapaonline02);
        tv03 = (TextView) findViewById(R.id.tvmapaonline03);

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        Log.d("Entrega", String.valueOf(laruta));

        String nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));
        tv01.setText(nombre_ruta);
        String descripcion_ruta = String.valueOf(laruta.get(0).get("descripcion_ruta"));
        tv02.setText(descripcion_ruta);
        String tiempo_estandar = String.valueOf(laruta.get(0).get("tiempo_estandar"));
        tv03.setText("Tiempo estimado del recorrido "+tiempo_estandar+ " segundos");

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fabmapaonline01);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DescargarCoordenadas();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fabmapaonline02);
        fab2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                new EnviarDatos(MapaOnlineActivity.this).execute();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        Log.d("Entrega", String.valueOf(laruta));

        String id_ruta = String.valueOf(laruta.get(0).get("id_ruta"));
        if (Build.VERSION.SDK_INT >= 10){
            StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(policy);
        }
        try{
            params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            usuario = new DefaultHttpClient(params);
            post = new HttpPost("http://181.75.202.177/WebService/MostrarCoordenadas.php");
            lista = new ArrayList<>();
            lista.add(new BasicNameValuePair("id_ruta", String.valueOf(id_ruta)));
            post.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));
            response = usuario.execute(post);
            entity = response.getEntity();
            responseString = EntityUtils.toString(entity);
            JSONObject object = new JSONObject(responseString);
            JSONArray responseJSON = object.getJSONArray("coordenadas");
            Log.w("Array: ", String.valueOf(responseJSON));
            for (int i=0; i<responseJSON.length(); i++){
                JSONObject o = responseJSON.getJSONObject(i);

                if (o.getString("posicion").equals("1")){
                    latitud = Double.valueOf(String.valueOf(o.get("latitud")));
                    longitud = Double.valueOf(String.valueOf(o.get("longitud")));

                    LatLng inicio = new LatLng(latitud, longitud);

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(inicio));
                    mMap.setMinZoomPreference(17);
                    mMap.setMaxZoomPreference(17);
                }

                latitud = Double.valueOf(String.valueOf(o.get("latitud")));
                longitud = Double.valueOf(String.valueOf(o.get("longitud")));
                LatLng casa = new LatLng(latitud, longitud);
                if(coordenadas.size() >= 1){
                    if(ultimoMarcador != null){
                        ultimoMarcador.remove();
                    }
                    ultimoMarcador = mMap.addMarker(new MarkerOptions().position(casa).title("final"));
                } else {
                    mMap.addMarker(new MarkerOptions().position(casa).title("").snippet("lat: " + casa));
                }
                MarkerOptions nuevoMarcador =  new MarkerOptions().anchor(0.0f, 0.7f)
                        .position(casa)
                        .draggable(false);

                if (viejoMarcador == null){
                    viejoMarcador = nuevoMarcador;
                }else {
                    mMap.addPolyline(new PolylineOptions().add(//traza la linea desde el viejomarcador hasta el nuevo marcador
                            viejoMarcador.getPosition(),
                            nuevoMarcador.getPosition()
                            )
                                    .width(3) //ancho
                                    .color(Color.BLACK)  //color
                    );
                    viejoMarcador = nuevoMarcador;
                }

                ContentValues datos = new ContentValues();
                datos.put("latitud", String.valueOf(o.get("latitud")));
                datos.put("longitud", String.valueOf(o.get("longitud")));
                datos.put("posicion", String.valueOf(o.get("posicion")));
                datos.put("estado_coordenada", String.valueOf(o.get("estado_coordenada")));
                coordenadas.add(datos);

            }
            Log.w("coordenadas", String.valueOf(coordenadas));
        } catch (IOException ex){
            ex.printStackTrace();
        } catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    private void DescargarRuta(){
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        Log.d("Entrega", String.valueOf(laruta));

        String nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));
        String descripcion_ruta = String.valueOf(laruta.get(0).get("descripcion_ruta"));
        String distancia_final = String.valueOf(laruta.get(0).get("distancia"));
        String tiempo_estandar = String.valueOf(laruta.get(0).get("tiempo_estandar"));

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        ContentValues registror = new ContentValues();

        registror.put("nombre_ruta", nombre_ruta);
        registror.put("id_sector", "1");
        registror.put("descripcion_ruta", descripcion_ruta);
        registror.put("distancia_final", distancia_final);
        registror.put("tiempo_estandar", tiempo_estandar);
        registror.put("estado", "1");

        bd.insert("datosruta", null, registror);
        bd.close();
    }

    private void DescargarCoordenadas(){
        DescargarRuta();

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        Log.d("Entrega", String.valueOf(laruta));

        String nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        Cursor fila = bd.rawQuery("select id_ruta from datosruta where nombre_ruta ='"+nombre_ruta+"'", null);
        if (fila.moveToFirst()) {
            int id_ruta = Integer.valueOf(fila.getString(0));
            for (int i =0; i <coordenadas.size(); i++){
                String latitud = coordenadas.get(i).get("latitud").toString();
                String longitud = coordenadas.get(i).get("longitud").toString();
                String posicion = coordenadas.get(i).get("posicion").toString();
                String estado_coordenada = coordenadas.get(i).get("estado_coordenada").toString();

                ContentValues registroc = new ContentValues();
                registroc.put("latitud", latitud);
                registroc.put("longitud", longitud);
                registroc.put("id_ruta", id_ruta);
                registroc.put("posicion", posicion);
                registroc.put("estado_coordenada", estado_coordenada);
                bd.insert("coordenadasruta", null, registroc);
            }
            bd.close();
        }
    }

    private boolean AgregarFavorito(){
        usuario = new DefaultHttpClient();
        post = new HttpPost("http://181.75.202.177/WebService/InsertarFavoritos.php");
        lista = new ArrayList<>();

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        String id_ruta = String.valueOf(laruta.get(0).get("id_ruta"));

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        Cursor fila = bd.rawQuery("select * from usuario", null);
        if (fila.moveToFirst()) {
            String id_usuario = fila.getString(3);

            lista.add(new BasicNameValuePair("id_ruta", id_ruta));
            lista.add(new BasicNameValuePair("id_usuario", id_usuario));

            try{
                post.setEntity(new UrlEncodedFormEntity(lista));
                usuario.execute(post);
                return true;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void onBackPressed() {
        int Estado = 2;
        ContentValues dato = new ContentValues();
        dato.put("estado", Estado);
        estado.add(dato);
        Intent intent = new Intent(MapaOnlineActivity.this, NavegacionUsuarioActivity.class);
        intent.putExtra("Estados", estado);
        startActivity(intent);
        this.finish();
    }
}
