package com.example.salatiel.tesis.Administrador;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.salatiel.tesis.R;

public class ListaRutasFragment extends Fragment {

    private Spinner spnrutas01;
    private final static String[] tipos = { "General", "Nuevas", "En revisiÃ³n" };
    private TextView textViewtiempo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_rutas, container, false);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipos);

        spnrutas01 = (Spinner) view.findViewById(R.id.spnrlistarutas01);
        spnrutas01.setAdapter(adapter);


        return view;
    }
}
