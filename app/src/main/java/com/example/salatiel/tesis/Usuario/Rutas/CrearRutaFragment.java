package com.example.salatiel.tesis.Usuario.Rutas;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.InicioActivity;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.NavegacionUsuarioActivity;

import java.util.ArrayList;

public class CrearRutaFragment extends Fragment {

    private Spinner spnrsector;
    private EditText txt01, txt02;
    private Button btn01, btn02;
    private ArrayList<ContentValues> preruta = new ArrayList<ContentValues>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crear_ruta, container, false);

        spnrsector = (Spinner) view.findViewById(R.id.spnrcrear01);

        txt01 = (EditText) view.findViewById(R.id.etcrear01);
        txt02 = (EditText) view.findViewById(R.id.etcrear02);

        btn01 = (Button) view.findViewById(R.id.btncrear01);
        btn02 = (Button) view.findViewById(R.id.btncrear02);

        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txt01.getText()) || (TextUtils.isEmpty(txt02.getText()))) {
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.contenedor, new RutasFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();
                }else {
                    txt01.setText("");
                    txt02.setText("");

                    Toast.makeText(getContext(), "Campos vacios", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ArrayList sector = new ArrayList();

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
        SQLiteDatabase bds = admin.getWritableDatabase();

        Cursor fila = bds.rawQuery("select * from sector", null);

        if (fila.moveToFirst()) {
            do {
                sector.add(fila.getString(1));
            } while(fila.moveToNext());
        }
        bds.close();

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sector);

        spnrsector.setAdapter(adaptador);

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txt01.getText()) || (TextUtils.isEmpty(txt02.getText()))){
                    Toast.makeText(getActivity(), "Hay campos sin completar", Toast.LENGTH_SHORT).show();
                }else{
                    InicioSQLiteOpenHelper adminu = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
                    SQLiteDatabase bd = adminu. getWritableDatabase();

                    String id_sectors = spnrsector.getSelectedItem().toString();

                    Cursor fila = bd.rawQuery("select id_sector from sector where nombre_sector='"+id_sectors+"'", null);

                    if (fila.moveToFirst()) {
                        int id_sector1 = Integer.valueOf(fila.getString(0));

                        String nombre_ruta = txt01.getText().toString().trim();
                        int id_sector = id_sector1;
                        String descripcion_ruta = txt02.getText().toString().trim();

                        Log.d("nombre", nombre_ruta);
                        Log.d("id", String.valueOf(id_sector));
                        Log.d("descripcion", descripcion_ruta);

                        ContentValues dato = new ContentValues();
                        dato.put("nombre_ruta", nombre_ruta);
                        dato.put("id_sector", id_sector);
                        dato.put("descripcion_ruta", descripcion_ruta);

                        preruta.add(dato);

                        Log.w("PUNTOS", preruta.toString());

                        bd.close();
                    }
                    Intent intent01 = new Intent(getActivity(), RegistroMapActivity.class);
                    intent01.putExtra("LaRuta", preruta);
                    startActivityForResult(intent01, 0);
                }
            }
        });

        return view;
    }
}
