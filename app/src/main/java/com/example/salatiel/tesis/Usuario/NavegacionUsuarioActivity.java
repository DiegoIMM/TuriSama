package com.example.salatiel.tesis.Usuario;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.InicioActivity;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.Rutas.RutasFragment;
import com.example.salatiel.tesis.Usuario.Sugerencias.SugerenciasFragment;

import java.util.ArrayList;

public class NavegacionUsuarioActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegacion_usuario);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("Estados");
        int Estado = Integer.parseInt(laruta.get(0).get("estado").toString());

        if (Estado == 1){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contenedor, new PrincipalFragment()).commit();
        } else if (Estado == 2) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contenedor, new RutasFragment()).commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navegacion_usuario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.accion_cerrarsesion) {
            final CharSequence[] items = new CharSequence[2];

            items[0] = "si";
            items[1] = "no";

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("¿Seguro que desea cerrar sesion?");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (item == 0) {
                        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getApplication(), "administracion", null, 1);
                        SQLiteDatabase bd = admin.getWritableDatabase();

                        bd.delete("usuario", null, null);
                        bd.close();

                        Intent intent01 = new Intent(NavegacionUsuarioActivity.this, InicioActivity.class);
                        startActivityForResult(intent01, 0);
                    } else {
                        dialog.cancel();
                    }
                }
            });
            AlertDialog alerta = builder.create();
            alerta.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_principal) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new PrincipalFragment()).commit();
        } else if (id == R.id.nav_rutas) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new RutasFragment()).commit();
        } else if (id == R.id.nav_record) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new RecordFragment()).commit();
        } else if (id == R.id.nav_lugares) {
            Intent intent001 = new Intent(NavegacionUsuarioActivity.this, LugaresRecomendadosActivity.class);
            startActivityForResult(intent001,0);
        } else if (id == R.id.nav_sugerencias) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new SugerenciasFragment()).commit();
        }  else if (id == R.id.nav_quienessomos) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, new QuienesSomosFragment()).commit();
        }else if (id == R.id.nav_sms_emergencia) {
            Intent intent01 = new Intent(NavegacionUsuarioActivity.this, SMSemergenciaActivity.class);
            startActivityForResult(intent01,0);
        }else if (id == R.id.nav_llamadas_emergencia) {
            Intent intent02 = new Intent(NavegacionUsuarioActivity.this, LlamadasemergenciaActivity.class);
            startActivityForResult(intent02 ,0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onBackPressed() {
        final CharSequence[] items = new CharSequence[2];

        items[0] = "si";
        items[1] = "no";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("¿Seguro que desea salir?");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent06 = new Intent(Intent.ACTION_MAIN);
                    intent06.addCategory(Intent.CATEGORY_HOME);
                    intent06.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent06);
                } else {
                    dialog.cancel();
                }
            }
        });
        AlertDialog alerta = builder.create();
        alerta.show();
    }
}
