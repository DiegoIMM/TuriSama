package com.example.salatiel.tesis.Usuario;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.salatiel.tesis.R;

public class LlamadasemergenciaActivity extends FragmentActivity {

    private Button btn01,btn02,btn03;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llamadas_emergencia);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1000);
        }

        btn01 = (Button) findViewById(R.id.imbtnambulancia);
        btn02 = (Button) findViewById(R.id.imbtnbomberos);
        btn03 = (Button) findViewById(R.id.imbtncarabineros);


        btn01.setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {

            Intent intent01 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:131"));
            Toast.makeText(getApplicationContext(), "Llamando a Ambulancia", Toast.LENGTH_SHORT).show();
            if (ActivityCompat.checkSelfPermission(LlamadasemergenciaActivity.this,Manifest.permission.CALL_PHONE)!=
                    PackageManager.PERMISSION_GRANTED)
                return;
            startActivity(intent01);
            }
        });
        btn02.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent02 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:132"));
                Toast.makeText(getApplicationContext(), "Llamando a Bomberos", Toast.LENGTH_SHORT).show();
                if (ActivityCompat.checkSelfPermission(LlamadasemergenciaActivity.this,Manifest.permission.CALL_PHONE)!=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(intent02);
            }
        });
        btn03.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent03 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:133"));
                Toast.makeText(getApplicationContext(), "Llamando a Carabineros", Toast.LENGTH_SHORT).show();
                if (ActivityCompat.checkSelfPermission(LlamadasemergenciaActivity.this,Manifest.permission.CALL_PHONE)!=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(intent03);
            }
        });
    }
}


