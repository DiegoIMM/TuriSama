package com.example.salatiel.tesis.Usuario;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salatiel.tesis.R;

public class SMSemergenciaActivity extends FragmentActivity implements LocationListener {
    protected LocationManager locationManager;
    TextView coordenadas;
    private EditText numeroE;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_emergencia);

        Toast.makeText(getApplicationContext(), "Recuerde activar GPS", Toast.LENGTH_SHORT).show();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1000, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1000, this);
        }

        button = (Button) findViewById(R.id.btnEnviar);

        button.setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {
                try {
                    TextView co = (TextView) findViewById(R.id.idCoordenadas);
                    String messageToSend = co.getText().toString();

                    if (ContextCompat.checkSelfPermission(SMSemergenciaActivity.this,
                            Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(SMSemergenciaActivity.this,
                                Manifest.permission.SEND_SMS)) {
                            ActivityCompat.requestPermissions(SMSemergenciaActivity.this,
                                    new String[]{Manifest.permission.SEND_SMS}, 1);

                        }else{
                            ActivityCompat.requestPermissions(SMSemergenciaActivity.this,
                                    new String[]{Manifest.permission.SEND_SMS}, 1);


                            numeroE = (EditText) findViewById(R.id.etNumero);

                            String n = numeroE.getText().toString();
                            SmsManager.getDefault().sendTextMessage(n, null,"\n" +
                                    messageToSend, null, null);

                            Toast.makeText(getApplicationContext(), "Mensaje Enviado!",
                                    Toast.LENGTH_LONG).show();

                            //Log.i("Avance", "Pasó aqui");
                        }
                    }else {

                        numeroE = (EditText) findViewById(R.id.etNumero);

                        String number = numeroE.getText().toString();
                        SmsManager.getDefault().sendTextMessage(number, null, "\n" +
                                messageToSend, null, null);

                        Toast.makeText(getApplicationContext(), "Mensaje Enviado!",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Fallo el envio!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onLocationChanged(Location location) {
        coordenadas = (TextView) findViewById(R.id.idCoordenadas);
        coordenadas.setText("Mi ubicacion actual es: " + "\n" +
                Html.fromHtml(location.getLatitude()+", "+location.getLongitude()));
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }
    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }
}


