package com.example.salatiel.tesis.Administrador;

import android.content.ContentValues;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.salatiel.tesis.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SugerenciasAdministradorFragment extends Fragment {

    private Spinner spnrsugerencias;
    private final static String[] tipos = { "General", "Editar ruta", "Deshabilitar ruta", "Sugerencias para app" };
    private TextView textViewtiempo;
    private Button btn01;
    private int tipo_sugerencia;
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post1;
    private List<NameValuePair> lista;
    private String responseString;
    private HttpResponse response;
    private HttpEntity entity;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sugerencias_administrador, container, false);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipos);

        listView = (ListView) view.findViewById(R.id.lvsugerenciasadministrador01);

        spnrsugerencias = (Spinner) view.findViewById(R.id.spnrsugerenciasadministrador01);
        spnrsugerencias.setAdapter(adapter);

        btn01 = (Button) view.findViewById(R.id.btnsugerenciasadministrador01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spnrsugerencias.getSelectedItem().equals("General")){
                    tipo_sugerencia = 0;
                } else if (spnrsugerencias.getSelectedItem().equals("Editar ruta")){
                    tipo_sugerencia = 1;
                } else if (spnrsugerencias.getSelectedItem().equals("Deshabilitar ruta")){
                    tipo_sugerencia = 2;
                } else if (spnrsugerencias.getSelectedItem().equals("Sugerencias para app")){
                    tipo_sugerencia = 3;
                }

                final ArrayList sugerencias = new ArrayList();

                if (Build.VERSION.SDK_INT >= 10){
                    StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                    StrictMode.setThreadPolicy(policy);
                }
                try{
                    params = new BasicHttpParams();
                    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

                    usuario = new DefaultHttpClient(params);
                    post1 = new HttpPost("http://181.75.202.177/WebService/MostrarSugerenciasAdmin.php");

                    lista = new ArrayList<>();
                    lista.add(new BasicNameValuePair("estado", String.valueOf(tipo_sugerencia)));

                    post1.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));

                    response = usuario.execute(post1);
                    entity = response.getEntity();

                    responseString = EntityUtils.toString(entity);

                    Log.d("Mensaje", responseString);
                    JSONObject object = new JSONObject(responseString);

                    JSONArray responseJSON = object.getJSONArray("sugerencia");

                    for (int i=0; i<responseJSON.length(); i++) {
                        JSONObject o = responseJSON.getJSONObject(i);

                        sugerencias.add(o.get("nombre_sugerencia"));
                    }
                    final ArrayAdapter<ContentValues> adaptador = new ArrayAdapter<ContentValues>(getContext(), android.R.layout.simple_list_item_1, sugerencias);
                    listView.setAdapter(adaptador);

                } catch (JSONException ex){
                    ex.printStackTrace();
                } catch (IOException ex){
                    ex.printStackTrace();
                }
            }
        });
        return view;
    }
}
