package com.example.salatiel.tesis.Usuario.Rutas;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.NavegacionUsuarioActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class RegistroMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView mensaje1, mensaje2;
    Double latitud = null;
    Double longitud = null;
    private MarkerOptions viejoMarcador = null;
    private Marker ultimoMarcador;
    private ArrayList<ContentValues> puntos = new ArrayList<ContentValues>();
    Timer timer = new Timer();
    Handler mHandler;
    private Button btn01, btn02, btn03;
    private int estado_coordenada = 0;
    private float Distancia, DistanciaTotal, DistanciaFinal;
    private Location PuntoA, PuntoB;
    private ArrayList<ContentValues> estado = new ArrayList<ContentValues>();
    Long tInicio, tFinal, tDiferencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_map);

        mensaje1 = (TextView) findViewById(R.id.tvmain01);
        mensaje2 = (TextView) findViewById(R.id.tvmain02);

        btn01 = (Button) findViewById(R.id.btnregistromap01);
        btn02 = (Button) findViewById(R.id.btnregistromap02);
        btn03 = (Button) findViewById(R.id.btnregistromap03);

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fabregistromap01);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (estado_coordenada == 0){
                    estado_coordenada = 1;
                } else if (estado_coordenada == 1) {
                    estado_coordenada = 0;
                }
            }
        });

        btn03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRuta();

                ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
                String nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));

                InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(v.getContext(), "administracion", null, 1);
                SQLiteDatabase bd = admin.getWritableDatabase();

                Cursor fila = bd.rawQuery("select id_ruta from datosruta where nombre_ruta ='"+nombre_ruta+"'", null);

                if (fila.moveToFirst()) {
                    int id_ruta = Integer.valueOf(fila.getString(0));

                    for (int i =0; i <puntos.size(); i++){
                        int posicion = Integer.valueOf(puntos.get(i).get("id").toString());
                        String latitud_ruta = puntos.get(i).get("lat").toString();
                        String longitud_ruta = puntos.get(i).get("lng").toString();
                        int estado = Integer.valueOf(puntos.get(i).get("estado").toString());

                        Log.d("Latitud", latitud_ruta);
                        Log.d("Longitud", longitud_ruta);

                        ContentValues registroc = new ContentValues();

                        registroc.put("latitud", latitud_ruta);
                        registroc.put("longitud", longitud_ruta);
                        registroc.put("id_ruta", id_ruta);
                        registroc.put("posicion", posicion);
                        registroc.put("estado_coordenada", estado);

                        bd.insert("coordenadasruta", null, registroc);
                    }
                }
                bd.close();

                int Estado = 2;
                ContentValues dato = new ContentValues();
                dato.put("estado", Estado);

                estado.add(dato);
                Intent intent02 = new Intent(v.getContext(), NavegacionUsuarioActivity.class);
                intent02.putExtra("Estados", estado);
                startActivityForResult(intent02, 0);
            }
        });

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        UiSettings btnZoom = mMap.getUiSettings();
        btnZoom.setZoomControlsEnabled(true);
    }

    private void tomarPosicion(){
        latitud = Double.parseDouble(mensaje1.getText().toString());
        longitud = Double.parseDouble(mensaje2.getText().toString());

        LatLng casa = new LatLng(latitud, longitud);

        if(puntos.size() >= 1){
            if(ultimoMarcador != null){
                ultimoMarcador.remove();
            }
            ultimoMarcador = mMap.addMarker(new MarkerOptions().position(casa).title("").snippet("lat: " + casa));
        } else {
            mMap.addMarker(new MarkerOptions().position(casa).title("").snippet("lat: " + casa));
        }

        MarkerOptions nuevoMarcador =  new MarkerOptions().anchor(0.0f, 0.7f)
                .position(casa)
                .draggable(false);

        if (viejoMarcador == null){
            viejoMarcador = nuevoMarcador;

        }else {
            mMap.addPolyline(new PolylineOptions().add(//traza la linea desde el viejomarcador hasta el nuevo marcador

                    viejoMarcador.getPosition(),
                    nuevoMarcador.getPosition()
                    )
                            .width(3) //ancho
                            .color(Color.BLACK)  //color
            );
            PuntoA = new Location("PuntoA");
            PuntoA.setLatitude(viejoMarcador.getPosition().latitude);
            PuntoA.setLongitude(viejoMarcador.getPosition().longitude);

            PuntoB = new Location("PuntoB");
            PuntoB.setLatitude(nuevoMarcador.getPosition().latitude);
            PuntoB.setLongitude(nuevoMarcador.getPosition().longitude);

            Distancia = PuntoA.distanceTo(PuntoB);

            viejoMarcador = nuevoMarcador;
        }

        ContentValues punto = new ContentValues();

        DistanciaTotal = DistanciaTotal + Distancia;

        punto.put("id", puntos.size() + 1);
        punto.put("lat", nuevoMarcador.getPosition().latitude);
        punto.put("lng", nuevoMarcador.getPosition().longitude);
        punto.put("estado", estado_coordenada);

        puntos.add(punto);

        Log.w("STring algo: ", String.valueOf(puntos));
    }

    //Apartir de aqui empezamos a obtener la direciones y coordenadas
    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setRegistroMapActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        mensaje1.setText("Latitud");
        mensaje2.setText("Longitud");
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        RegistroMapActivity registroMapActivity;

        public RegistroMapActivity getRegistroMapActivity() {
            return registroMapActivity;
        }

        public void setRegistroMapActivity(RegistroMapActivity registroMapActivity) {
            this.registroMapActivity = registroMapActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            loc.getLatitude();
            loc.getLongitude();

            String Text1 = ""+loc.getLatitude();
            mensaje1.setText(Text1);

            String Text2 = ""+loc.getLongitude();
            mensaje2.setText(Text2);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            mensaje1.setText("GPS Desactivado");
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            mensaje1.setText("GPS Activado");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }

    public void IniciarRegistro(View view) {
        Log.d("repite", "1");
        tInicio = System.currentTimeMillis();
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Toast.makeText(getApplicationContext(), "Calculando posicion", Toast.LENGTH_SHORT).show();
                tomarPosicion();
            }
        };

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Message message = mHandler.obtainMessage();
                message.sendToTarget();
            }
        }, 10*1000, 10*1000);
        btn01.setEnabled(false);
        btn02.setEnabled(true);
    }

    public void DetenerRegistro(View view){
        timer.cancel();
        Toast.makeText(getApplicationContext(), "Ruta finalizada", Toast.LENGTH_SHORT).show();
        btn02.setEnabled(false);
        btn03.setEnabled(true);

        tFinal = System.currentTimeMillis();

        DistanciaFinal = DistanciaTotal;
        Toast.makeText(getApplicationContext(), "Tu Distancia Recorrida es: " +DistanciaTotal, Toast.LENGTH_SHORT).show();
    }

    private void GuardarRuta(){
        tDiferencia = tFinal - tInicio;
        Double SegundosTranscurridos = tDiferencia / 1000.0;
        SegundosTranscurridos = SegundosTranscurridos /60;

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");

        String nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));
        int id_sector = Integer.valueOf(laruta.get(0).get("id_sector").toString());
        String descripcion_ruta = String.valueOf(laruta.get(0).get("descripcion_ruta"));

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Log.d("String1: ", nombre_ruta);
        Log.d("String2: ", String.valueOf(id_sector));
        Log.d("String3: ", descripcion_ruta);

        ContentValues registror = new ContentValues();

        Log.d("TIEMPO", String.valueOf(SegundosTranscurridos));
        registror.put("nombre_ruta", nombre_ruta);
        registror.put("id_sector", id_sector);
        registror.put("descripcion_ruta", descripcion_ruta);
        registror.put("distancia_final", DistanciaFinal);
        registror.put("tiempo_estandar", SegundosTranscurridos);
        registror.put("estado", "0");

        bd.insert("datosruta", null, registror);
        bd.close();
    }
}