package com.example.salatiel.tesis.Administrador;

import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.salatiel.tesis.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MarcadorActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Location marcador;
    private Button btn01, btn02;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marcador);

        btn01 = (Button) findViewById(R.id.btninsertarmarcador01);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //accede a las configuraciones y muestra boton de zoom (+ y -)
        UiSettings btnZoom = mMap.getUiSettings();
        btnZoom.setZoomControlsEnabled(true);

        // aÃ±ade marcador y agrega zoom a ninhue
        LatLng casa = new LatLng(-36.395137, -72.400056);
        mMap.addMarker(new MarkerOptions().position(casa).title("Kiuuu").snippet("lat: " +casa));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(casa));
        float zoomlevel = 20;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(casa, zoomlevel));


        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter(){

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.info_window, null);

                TextView tvLocality = (TextView) v.findViewById(R.id.tv_locality);
                //  TextView tvLat = (TextView) findViewById(R.id.tv_lat);
                TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);
                TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);

                LatLng ll = marker.getPosition();
                tvLocality.setText(marker.getTitle());
                //   tvLat.setText("algo");
                tvLng.setText("Longitude: ");
                tvSnippet.setText(marker.getSnippet());

                return v;
            }
        });
    }
}
