package com.example.salatiel.tesis.Administrador;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.salatiel.tesis.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class AgregarMarcadorActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Spinner spnrmarcador01;
    private final static String[] tipos = { "Centro medico", "Monumento historico", "Hospedaje", "Gastronomia",
            "Supermercado","Zona de fotografia", "Zona de WIFI", "Zona de recreacion"};
    private Double latitud = null;
    private Double longitud = null;
    private EditText txt01;
    private Button btn01;
    private HttpClient usuario;
    private HttpPost post;
    private List<NameValuePair> lista;
    int tipo_sugerencia;

    private boolean datos(){
        if (spnrmarcador01.getSelectedItem().equals("Centro medico")){
            tipo_sugerencia = 1;
        } else if (spnrmarcador01.getSelectedItem().equals("Monumento historico")){
            tipo_sugerencia = 2;
        } else if (spnrmarcador01.getSelectedItem().equals("Hospedaje")){
            tipo_sugerencia = 3;
        } else if (spnrmarcador01.getSelectedItem().equals("Gastronomia")){
            tipo_sugerencia = 4;
        } else if (spnrmarcador01.getSelectedItem().equals("Supermercado")){
            tipo_sugerencia = 5;
        } else if (spnrmarcador01.getSelectedItem().equals("Zona de fotografia")){
            tipo_sugerencia = 6;
        } else if (spnrmarcador01.getSelectedItem().equals("Zona de WIFI")){
            tipo_sugerencia = 7;
        } else if (spnrmarcador01.getSelectedItem().equals("Zona de recreacion")){
            tipo_sugerencia = 8;
        }

        usuario = new DefaultHttpClient();
        post = new HttpPost("http://181.75.202.177/WebService/InsertarLocalidad.php");
        lista = new ArrayList<NameValuePair>(5);

        lista.add(new BasicNameValuePair("nombre", txt01.getText().toString().trim()));
        lista.add(new BasicNameValuePair("tipo", String.valueOf(tipo_sugerencia)));
        lista.add(new BasicNameValuePair("id", "1"));
        lista.add(new BasicNameValuePair("latitud", String.valueOf(latitud)));
        lista.add(new BasicNameValuePair("longitud", String.valueOf(longitud)));


        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_marcador);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tipos);
        spnrmarcador01 = (Spinner) findViewById(R.id.spnriconosmarcadores01);
        spnrmarcador01.setAdapter(adapter);

        txt01 = (EditText) findViewById(R.id.etagregarmarcador01);

        btn01 = (Button) findViewById(R.id.btnagregarmarcador01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txt01.getText())){
                    Toast.makeText(getApplicationContext(), "Nombre de la localidad no asignado", Toast.LENGTH_SHORT).show();
                } else if (latitud == null && longitud == null){
                        Toast.makeText(getApplicationContext(), "No a seleccionado ninguna localidad", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("nombre", txt01.getText().toString());
                        Log.d("latitud", String.valueOf(latitud));
                        Log.d("longitud", String.valueOf(longitud));
                    }
                }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings btnZoom = mMap.getUiSettings();
        btnZoom.setZoomControlsEnabled(true);

        LatLng casa = new LatLng(-36.395137, -72.400056);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(casa));
        float zoomlevel = 15;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(casa, zoomlevel));

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                MarkerOptions nuevoMarcador =  new MarkerOptions().anchor(0.0f, 0.7f)
                        .position(latLng)
                        .draggable(false);

                mMap.addMarker(new MarkerOptions().position(latLng));

                latitud = nuevoMarcador.getPosition().latitude;
                longitud = nuevoMarcador.getPosition().longitude;
            }
        });
    }
}
