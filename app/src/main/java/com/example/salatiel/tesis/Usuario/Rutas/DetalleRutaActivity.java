package com.example.salatiel.tesis.Usuario.Rutas;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.NavegacionUsuarioActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class DetalleRutaActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView tv01, tv02, tv03;
    Double latitud = null;
    Double longitud = null;
    private MarkerOptions viejoMarcador = null;
    private Marker ultimoMarcador;
    private ArrayList<ContentValues> puntos = new ArrayList<ContentValues>();
    private ArrayList<ContentValues> estado = new ArrayList<ContentValues>();
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post1, post2, post3;
    private List<NameValuePair> lista1, lista2, lista3;
    private String nombre_ruta, descripcion_ruta, id_usuario, id_sector, distancia_final, tiempo_estimado,
            bid_ruta, responseString3;
    private HttpResponse response;
    private HttpEntity entity;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_ruta);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tv01 = (TextView) findViewById(R.id.tvmapaonline01);
        tv02 = (TextView) findViewById(R.id.tvdetalleruta02);
        tv03 = (TextView) findViewById(R.id.tvdetalleruta03);

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");

        nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));
        id_sector = String.valueOf(laruta.get(0).get("id_sector"));
        descripcion_ruta = String.valueOf(laruta.get(0).get("descripcion_ruta"));
        distancia_final = String.valueOf(laruta.get(0).get("distancia_final"));
        tiempo_estimado = String.valueOf(laruta.get(0).get("tiempo_estandar"));

        tv01.setText(nombre_ruta);
        tv02.setText(descripcion_ruta);
        tv03.setText("Tiempo estimado del recorrido "+tiempo_estimado+ " minutos");

        class EnviarDatosRuta extends AsyncTask<String, String, String> {
            private Activity contexto;

            EnviarDatosRuta(Activity context){
                this.contexto = context;
            }

            @Override
            protected String doInBackground(String... params) {
                if (SubirRuta()){
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ObtenerIDRuta();
                        }
                    });
                }else {
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos no enviados", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return null;
            }
        }

        class EnviarCoordenadasRuta extends AsyncTask<String, String, String> {
            private Activity contexto;

            EnviarCoordenadasRuta(Activity context){
                this.contexto = context;
            }

            @Override
            protected String doInBackground(String... params) {
                if (GuardarCoordenadas()){
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos enviados exitosamente", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else {
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos no enviados", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return null;
            }
        }

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fabmapaonline01);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EnviarDatosRuta(DetalleRutaActivity.this).execute();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new EnviarCoordenadasRuta(DetalleRutaActivity.this).execute();
                    }
                }, 2000);
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fabmapaonline02);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
                Intent intent2 = new Intent(getApplicationContext(), AmpliarRutaActivity.class);
                intent2.putExtra("LaRuta", laruta);
                startActivityForResult(intent2, 0);
            }
        });

        FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fabregistroruta03);
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertaEliminar();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from coordenadasruta where id_ruta='"+id_ruta+"'", null);
        if (fila.moveToFirst()) {
            if (fila.getString(4).equals("1")){
                latitud = Double.parseDouble(fila.getString(1));
                longitud = Double.parseDouble(fila.getString(2));

                LatLng inicio = new LatLng(latitud, longitud);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(inicio));
                mMap.setMinZoomPreference(17);
                mMap.setMaxZoomPreference(17);
            }
            do {
                latitud = Double.parseDouble(fila.getString(1));
                longitud = Double.parseDouble(fila.getString(2));

                LatLng casa = new LatLng(latitud, longitud);


                if(puntos.size() >= 1){
                    if(ultimoMarcador != null){
                        ultimoMarcador.remove();
                    }
                    ultimoMarcador = mMap.addMarker(new MarkerOptions().position(casa).title("inicio").snippet("" + casa));
                } else {
                    mMap.addMarker(new MarkerOptions().position(casa).title("").snippet("lat: " + casa));
                }

                MarkerOptions nuevoMarcador =  new MarkerOptions().anchor(0.0f, 0.7f)
                        .position(casa)
                        .draggable(false);

                if (viejoMarcador == null){
                    viejoMarcador = nuevoMarcador;

                }else {
                    mMap.addPolyline(new PolylineOptions().add(//traza la linea desde el viejomarcador hasta el nuevo marcador
                            viejoMarcador.getPosition(),
                            nuevoMarcador.getPosition()
                            )
                                    .width(3) //ancho
                                    .color(Color.BLACK)  //color
                    );
                    viejoMarcador = nuevoMarcador;
                }

                ContentValues punto = new ContentValues();
                punto.put("id", puntos.size() + 1);
                punto.put("lat", nuevoMarcador.getPosition().latitude);
                punto.put("lng", nuevoMarcador.getPosition().longitude);

                puntos.add(punto);

            } while(fila.moveToNext());



            Log.w("Puntos: ", String.valueOf(puntos));
        }
        bd.close();
    }

    private void EliminarRuta(){
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");

        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        bd.execSQL("delete from datosruta where id_ruta='"+id_ruta+"'");

        bd.execSQL("delete from coordenadasruta where id_ruta='"+id_ruta+"'");

        bd.close();
    }

    private void AlertaEliminar(){
        final CharSequence[] items = new CharSequence[2];

        items[0] = "Si";
        items[1] = "No";

        AlertDialog.Builder builder01 = new AlertDialog.Builder(this);
        builder01.setTitle("¿Seguro que desea eliminar esta ruta?");
        builder01.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    EliminarRuta();
                    Toast.makeText(getApplicationContext(), "Ruta eliminadda", Toast.LENGTH_SHORT).show();
                    int Estado = 2;

                    ContentValues dato = new ContentValues();

                    dato.put("estado", Estado);

                    estado.add(dato);
                    Intent intent = new Intent(getApplicationContext(), NavegacionUsuarioActivity.class);
                    intent.putExtra("Estados", estado);
                    startActivityForResult(intent, 0);
                    dialog.cancel();
                } else {
                    Toast.makeText(getApplicationContext(), "Ruta no eliminada", Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            }
        });
        AlertDialog alerta = builder01.create();
        alerta.show();
    }

    private boolean SubirRuta(){
        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from usuario", null);

        if (fila.moveToFirst()) {
            id_usuario = String.valueOf(fila.getString(2));

            if (Build.VERSION.SDK_INT >= 10) {
                StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                StrictMode.setThreadPolicy(policy);
            }
            params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            usuario = new DefaultHttpClient(params);
            post1 = new HttpPost("http://181.75.202.177/WebService/InsertarRuta.php");

            lista1 = new ArrayList<>();

            lista1.add(new BasicNameValuePair("nombre_ruta", nombre_ruta));
            lista1.add(new BasicNameValuePair("descripcion_ruta", descripcion_ruta));
            lista1.add(new BasicNameValuePair("tiempo_estandar", tiempo_estimado));
            lista1.add(new BasicNameValuePair("id_sector", id_sector));
            lista1.add(new BasicNameValuePair("distancia", distancia_final));
            lista1.add(new BasicNameValuePair("id_usuario", id_usuario));

            try {
                post1.setEntity(new UrlEncodedFormEntity(lista1, "UTF-8"));
                usuario.execute(post1);
                Log.d("listado: ", String.valueOf(lista1));
                return true;
            } catch (UnsupportedEncodingException ex){
                ex.printStackTrace();
            } catch (IOException ex){
                ex.printStackTrace();
            }
        }
        return false;
    }

    private boolean GuardarCoordenadas() {
        try {
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from coordenadasruta where id_ruta='"+id_ruta+"'", null);
        if (fila.moveToFirst()) {
            do {
                Log.d("latitud", fila.getString(1));
                Log.d("longitud", fila.getString(2));
                Log.d("id de ruta", String.valueOf(id_ruta));
                Log.d("posicion", fila.getString(4));
                Log.d("estado_coordenada", fila.getString(5));

                if (Build.VERSION.SDK_INT >= 10) {
                    StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                    StrictMode.setThreadPolicy(policy);
                }

                params = new BasicHttpParams();
                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

                usuario = new DefaultHttpClient(params);
                post3 = new HttpPost("http://181.75.202.177/WebService/InsertarCoordenadasRuta.php");

                lista3 = new ArrayList<>();

                lista3.add(new BasicNameValuePair("latitud", fila.getString(1)));
                lista3.add(new BasicNameValuePair("longitud", fila.getString(2)));
                lista3.add(new BasicNameValuePair("id_ruta", bid_ruta));
                lista3.add(new BasicNameValuePair("posicion", fila.getString(4)));
                lista3.add(new BasicNameValuePair("estado_coordenada", fila.getString(5)));

                    post3.setEntity(new UrlEncodedFormEntity(lista3, "UTF-8"));
                    usuario.execute(post3);
                    Log.d("listado: ", String.valueOf(lista3));
            }while (fila.moveToNext());
            return true;
        }
        } catch (UnsupportedEncodingException ex){
            ex.printStackTrace();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        return false;
    }

    private void ObtenerIDRuta(){
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");

        nombre_ruta = String.valueOf(laruta.get(0).get("nombre_ruta"));

        if (Build.VERSION.SDK_INT >= 10) {
            StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(policy);
        }
        try {
            params = new BasicHttpParams();

            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            usuario = new DefaultHttpClient(params);
            post3 = new HttpPost("http://181.75.202.177/WebService/MostrarIdRuta.php");

            lista2 = new ArrayList<>();
            lista2.add(new BasicNameValuePair("nombre_ruta", nombre_ruta));

            post3.setEntity(new UrlEncodedFormEntity(lista2, "UTF-8"));

            response = usuario.execute(post3);
            entity = response.getEntity();

            responseString3 = EntityUtils.toString(entity);

            JSONObject object = new JSONObject(responseString3);

            bid_ruta = object.getString("id_ruta");

            Log.v("Mensaje", responseString3);

        }catch (JSONException ex){
            ex.printStackTrace();
        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public void onBackPressed() {
        int Estado = 2;
        ContentValues dato = new ContentValues();
        dato.put("estado", Estado);
        estado.add(dato);
        Intent intent = new Intent(DetalleRutaActivity.this, NavegacionUsuarioActivity.class);
        intent.putExtra("Estados", estado);
        startActivity(intent);
        this.finish();
    }
}