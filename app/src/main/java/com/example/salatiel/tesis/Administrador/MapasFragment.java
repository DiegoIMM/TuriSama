package com.example.salatiel.tesis.Administrador;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.salatiel.tesis.R;

public class MapasFragment extends Fragment {

    private final static String[] tipos = { "General", "Nuevas Rutas", "Habilitadas", "Deshabilitadas" };
    private Spinner spnrtipo;
    private Button btn01, btn02;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapas, container, false);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipos);
        spnrtipo = (Spinner) view.findViewById(R.id.spnrmapas01);
        spnrtipo.setAdapter(adapter);

        btn01 = (Button) view.findViewById(R.id.btnmapas01);

        btn02 = (Button) view.findViewById(R.id.btnmapas02);
        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent01 = new Intent(getActivity(), AgregarMarcadorActivity.class);
                startActivityForResult(intent01, 0);
            }
        });

        return view;
    }
}
