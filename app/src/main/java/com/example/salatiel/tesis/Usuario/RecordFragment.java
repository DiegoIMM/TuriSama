package com.example.salatiel.tesis.Usuario;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;

import java.util.ArrayList;

public class RecordFragment extends Fragment {

    private Button btn01;
    private ListView listView;
    private ArrayList<ContentValues> record = new ArrayList<ContentValues>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record, container, false);

        listView = (ListView) view.findViewById(R.id.lvrecord01);

        btn01 = (Button) view.findViewById(R.id.btnrecord01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getActivity(), "administracion", null, 1);
                SQLiteDatabase bd = admin.getWritableDatabase();

                Cursor fila1 = bd.rawQuery("select * from record", null);
                if (fila1.moveToFirst()) {
                    do {
                        String id_ruta = fila1.getString(1);

                        Cursor fila2 = bd.rawQuery("select * from datosruta where id_ruta='"+id_ruta+"'", null);
                        if (fila2.moveToFirst()) {
                            String nombre_ruta = fila2.getString(1);
                            ContentValues datos = new ContentValues();
                            datos.put(fila2.getString(1), fila1.getString(3));
                            record.add(datos);
                            Log.d("en el array ", nombre_ruta);
                            Log.d("en el array ", String.valueOf(record));
                        }

                    } while (fila1.moveToNext());
                }

                final ArrayAdapter<ContentValues> adaptador = new ArrayAdapter<ContentValues>(getContext(), android.R.layout.simple_list_item_1, record);
                listView.setAdapter(adaptador);
                bd.close();
            }
        });

        return view;
    }
}
