package com.example.salatiel.tesis.Usuario.Rutas;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TabTotalRutasFragment extends Fragment {

    private Spinner spnrsector;
    private Button btn01;
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post;
    private HttpResponse response;
    private HttpEntity entity;
    private List<NameValuePair> lista;
    private String responseString;
    private ListView listView;
    private ArrayList<ContentValues> datosruta = new ArrayList<ContentValues>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_tab_total_rutas, container, false);

        listView = (ListView) view.findViewById(R.id.lvrutastotales01);

        spnrsector = (Spinner) view.findViewById(R.id.spnrrutastotales01);
        ArrayList sector = new ArrayList();
        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
        SQLiteDatabase bds = admin.getWritableDatabase();
        Cursor fila = bds.rawQuery("select * from sector", null);
        if (fila.moveToFirst()) {
            do {
                sector.add(fila.getString(1));
            } while(fila.moveToNext());
        }
        bds.close();
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sector);
        spnrsector.setAdapter(adaptador);

        btn01 = (Button) view.findViewById(R.id.btnrutastotales01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 10){
                    StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                    StrictMode.setThreadPolicy(policy);
                }

                try{
                    final ArrayList rutas = new ArrayList();

                    params = new BasicHttpParams();

                    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

                    usuario = new DefaultHttpClient(params);
                    post = new HttpPost("http://181.75.202.177/WebService/MostrarNombreRuta.php");

                    InicioSQLiteOpenHelper adminu = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
                    SQLiteDatabase bd = adminu. getWritableDatabase();

                    String id_sectors = spnrsector.getSelectedItem().toString();

                    Cursor fila = bd.rawQuery("select id_sector from sector where nombre_sector='"+id_sectors+"'", null);
                    if (fila.moveToFirst()) {
                        int id_sector = Integer.valueOf(fila.getString(0));

                        lista = new ArrayList<>();
                        lista.add(new BasicNameValuePair("id_sector", String.valueOf(id_sector)));
                        lista.add(new BasicNameValuePair("estado", "1"));
                    }

                    post.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));

                    response = usuario.execute(post);
                    entity = response.getEntity();

                    responseString = EntityUtils.toString(entity);
                    JSONObject object = new JSONObject(responseString);

                    JSONArray responseJSON = object.getJSONArray("rutas");
                    Log.w("Array: ", String.valueOf(responseJSON));

                    for (int i=0; i<responseJSON.length(); i++){
                        JSONObject o = responseJSON.getJSONObject(i);
                        rutas.add(o.get("nombre_ruta"));
                    }

                    final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, rutas);
                    listView.setAdapter(adaptador);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String nombre = adaptador.getItem(position);

                            if (Build.VERSION.SDK_INT >= 10){
                                StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                                StrictMode.setThreadPolicy(policy);
                            }

                            try {
                                params = new BasicHttpParams();
                                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                                usuario = new DefaultHttpClient(params);
                                post = new HttpPost("http://181.75.202.177/WebService/MostrarRutas.php");
                                lista = new ArrayList<>();
                                lista.add(new BasicNameValuePair("nombre_ruta", nombre));
                                post.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));
                                response = usuario.execute(post);
                                entity = response.getEntity();
                                responseString = EntityUtils.toString(entity);
                                JSONObject object = new JSONObject(responseString);
                                JSONArray responseJSON = object.getJSONArray("rutas");

                                for (int i=0; i<responseJSON.length(); i++){
                                    JSONObject o = responseJSON.getJSONObject(i);

                                    ContentValues datos = new ContentValues();

                                    datos.put("id_ruta", String.valueOf(o.get("id_ruta")));
                                    datos.put("nombre_ruta", nombre);
                                    datos.put("descripcion_ruta", String.valueOf(o.get("descripcion_ruta")));
                                    datos.put("dificultad", String.valueOf(o.get("dificultad")));
                                    datos.put("tiempo_estandar", String.valueOf(o.get("tiempo_estandar")));
                                    datos.put("id_usuario", String.valueOf(o.get("id_usuario")));
                                    datos.put("distancia", String.valueOf(o.get("distancia")));

                                    datosruta.add(datos);
                                }
                            } catch (IOException ex){
                                ex.printStackTrace();
                            } catch (JSONException ex){
                                ex.printStackTrace();
                            }

                            Intent intent01 = new Intent(getActivity(), MapaOnlineActivity.class);
                            intent01.putExtra("LaRuta", datosruta);
                            startActivityForResult(intent01, 0);
                        }
                    });

                } catch (IOException ex){
                    ex.printStackTrace();
                } catch (JSONException ex){
                    ex.printStackTrace();
                }
            }
        });

        return view;
    }
}