package com.example.salatiel.tesis.Usuario.Rutas;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;

import java.util.ArrayList;

public class TabRutasDescargadasFragment extends Fragment {

    private ArrayList<ContentValues> datosruta = new ArrayList<ContentValues>();
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_rutas_descargadas, container, false);

        listView = (ListView) view.findViewById(R.id.lvrutasdescargadas01);

        ArrayList rutas = new ArrayList();

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
        SQLiteDatabase bds = admin.getWritableDatabase();

        Cursor fila = bds.rawQuery("select * from datosruta where estado = 1", null);

        if (fila.moveToFirst()) {
            do {
                rutas.add(fila.getString(1));
            } while(fila.moveToNext());
            bds.close();
            final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, rutas);
            listView.setAdapter(adaptador);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String nombre = adaptador.getItem(position);
                    InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
                    SQLiteDatabase bdr = admin.getWritableDatabase();
                    Cursor fila = bdr.rawQuery("select * from datosruta where nombre_ruta='"+nombre+"'", null);
                    if (fila.moveToFirst()) {
                        int id_ruta = Integer.valueOf(fila.getString(0));
                        int id_sector = Integer.valueOf(fila.getString(2));
                        String descripcion_ruta = fila.getString(3);
                        String distancia_final = fila.getString(4);
                        String tiempo_estimado = fila.getString(5);
                        ContentValues datos = new ContentValues();
                        datos.put("id_ruta", id_ruta);
                        datos.put("nombre_ruta", nombre);
                        datos.put("id_sector", id_sector);
                        datos.put("descripcion_ruta", descripcion_ruta);
                        datos.put("distancia_final", distancia_final);
                        datos.put("tiempo_estandar", tiempo_estimado);
                        datosruta.add(datos);
                    }
                    bdr.close();
                    Intent intent01 = new Intent(getActivity(), DetalleRutaActivity.class);
                    intent01.putExtra("LaRuta", datosruta);
                    startActivityForResult(intent01, 0);
                }
            });
        }

        return view;
    }
}
