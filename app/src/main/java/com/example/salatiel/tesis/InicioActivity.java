package com.example.salatiel.tesis;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.database.Cursor;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.salatiel.tesis.Administrador.NavegacionAdministradorActivity;
import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.Usuario.NavegacionUsuarioActivity;
import com.example.salatiel.tesis.Usuario.RegistroActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class InicioActivity extends AppCompatActivity {

    private Button btn02;
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post1, post2;
    private List<NameValuePair> lista;
    private String nombre, contrasena, responseString, responseString2;
    private HttpResponse response;
    private HttpEntity entity;
    private ArrayList<ContentValues> estado = new ArrayList<ContentValues>();

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        btn02 = (Button) findViewById(R.id.btninicio02);

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from usuario", null);

        if (fila.moveToFirst()) {
            if (fila.getString(2).equals("1")) {
                int Estado = 1;
                ContentValues dato = new ContentValues();
                dato.put("estado", Estado);

                estado.add(dato);

                Intent intent3 = new Intent(InicioActivity.this, NavegacionUsuarioActivity.class);
                intent3.putExtra("Estados", estado);
                startActivity(intent3);
            } else if (fila.getString(2).equals("2")) {
                Intent intent3 = new Intent(InicioActivity.this, NavegacionAdministradorActivity.class);
                startActivity(intent3);
            }
        }
        bd.close();

        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent02 = new Intent(v.getContext(), RegistroActivity.class);
                startActivityForResult(intent02, 0);
            }
        });
    }

    public void onAlertDialog(View view) {
        final CharSequence[] items = new CharSequence[2];

        items[0] = "Recuperar nombre de usuario";
        items[1] = "Recuperar contraseña de usuario";

        AlertDialog.Builder builder01 = new AlertDialog.Builder(this);
        builder01.setTitle("Selecciona opcion que necesite");
        builder01.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Toast.makeText(getApplicationContext(), "El nombre de usuario fue enviado al E-mail", Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                } else {
                    Toast.makeText(getApplicationContext(), "La contraseña fue enviada al E-mail", Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            }
        });
        AlertDialog alerta = builder01.create();
        alerta.show();
    }

    public void InicioSesion(View view) {
        if (Build.VERSION.SDK_INT >= 10) {
            StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(policy);
        }

        nombre = ((EditText) findViewById(R.id.etinicio01)).getText().toString();
        contrasena = ((EditText) findViewById(R.id.etinicio02)).getText().toString();

        try {
            params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            usuario = new DefaultHttpClient(params);
            post1 = new HttpPost("http://181.75.202.177/WebService/Login.php");

            lista = new ArrayList<>();
            lista.add(new BasicNameValuePair("nombre", nombre));
            lista.add(new BasicNameValuePair("contrasena", contrasena));

            post1.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));

            response = usuario.execute(post1);
            entity = response.getEntity();

            responseString = EntityUtils.toString(entity);

            Log.d("Mensaje", responseString);
            JSONObject o = new JSONObject(responseString);

            if (o.getString("estado").equals("1")) {
                MostrarSector();

                int Estado = 1;
                ContentValues dato = new ContentValues();
                dato.put("estado", Estado);

                estado.add(dato);

                Intent intent01 = new Intent(view.getContext(), NavegacionUsuarioActivity.class);
                intent01.putExtra("Estados", estado);
                startActivityForResult(intent01, 0);

                Toast.makeText(getApplicationContext(), "Bienvenido " + nombre, Toast.LENGTH_LONG).show();

                InicioSQLiteOpenHelper adminu = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
                SQLiteDatabase bd = adminu. getWritableDatabase();

                String sqltoken = o.getString("token");
                String sqlnombre = o.getString("nombre");
                String sqlestado = o.getString("estado");
                String sqlid = o.getString("id");

                ContentValues registrou = new ContentValues();

                registrou.put("tokensql", sqltoken);
                registrou.put("nombresql", sqlnombre);
                registrou.put("estadosql", sqlestado);
                registrou.put("idsql", sqlid);
                Log.d("id", sqlid);

                bd.insert("usuario", null, registrou);
                bd.close();

            } else {
                if (o.getString("estado").equals("2")) {

                    Intent intent02 = new Intent(view.getContext(), NavegacionAdministradorActivity.class);
                    startActivityForResult(intent02, 0);

                    Toast.makeText(getApplicationContext(), "Bienvenido " + nombre, Toast.LENGTH_SHORT).show();

                    InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    String sqltoken = o.getString("token");
                    String sqlnombre = o.getString("nombre");
                    String sqlestado = o.getString("estado");

                    ContentValues registro = new ContentValues();

                    registro.put("tokensql", sqltoken);
                    registro.put("nombresql", sqlnombre);
                    registro.put("estadosql", sqlestado);

                    bd.insert("usuario", null, registro);
                    bd.close();

                } else {
                    Toast.makeText(getApplicationContext(), "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No a ingresado datos", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBackPressed() {
        final CharSequence[] items = new CharSequence[2];

        items[0] = "si";
        items[1] = "no";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("¿Seguro que desea salir?");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent06 = new Intent(Intent.ACTION_MAIN);
                    intent06.addCategory(Intent.CATEGORY_HOME);
                    intent06.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent06);
                } else {
                    dialog.cancel();
                }
            }
        });
        AlertDialog alerta = builder.create();
        alerta.show();
    }

    private void MostrarSector(){
        if (Build.VERSION.SDK_INT >= 10){
            StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
            StrictMode.setThreadPolicy(policy);
        }

        try{
            params = new BasicHttpParams();

            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            usuario = new DefaultHttpClient(params);
            post2 = new HttpPost("http://181.75.202.177/WebService/MostrarSector.php");

            response = usuario.execute(post2);
            entity = response.getEntity();

            responseString2 = EntityUtils.toString(entity);

            JSONObject object = new JSONObject(responseString2);

            Log.v("Mensaje", responseString2);

            JSONArray responseJSON = object.getJSONArray("sector");

            InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
            SQLiteDatabase bds = admin.getWritableDatabase();

            for (int i=0; i<responseJSON.length(); i++){
                JSONObject o = responseJSON.getJSONObject(i);

                int sqlid = Integer.valueOf(o.getString("id_sector"));
                String sqlnombre = o.getString("nombre_sector");
                String sqldescripcion = o.getString("descripcion_sector");

                ContentValues registro2 = new ContentValues();

                registro2.put("id_sector", sqlid);
                registro2.put("nombre_sector", sqlnombre);
                registro2.put("descripcion_sector", sqldescripcion);

                bds.insert("sector", null, registro2);
            }

            bds.close();

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "error 1", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error 2", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error 3", Toast.LENGTH_SHORT).show();
        }

    }
}

