package com.example.salatiel.tesis.Usuario;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;

import com.example.salatiel.tesis.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class LugaresRecomendadosActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    protected LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lugares_recomendados);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1000, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1000, this);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        UiSettings btnZoom = mMap.getUiSettings();
        btnZoom.setZoomControlsEnabled(true);


        LatLng Hogardemenores = new LatLng(-36.390297, -72.401922);
        mMap.addMarker(new MarkerOptions().position(Hogardemenores).title("Antiguo Hogar de Menores")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.lugardefotos)));

        LatLng Mastil = new LatLng(-36.393491, -72.408434);
        mMap.addMarker(new MarkerOptions().position(Mastil).title("Mástil")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.lugardefotos)));

        LatLng caminocuna = new LatLng(-36.395198, -72.426589);
        mMap.addMarker(new MarkerOptions().position(caminocuna).title("Zona de Fotografía")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.lugardefotos)));

        LatLng frentelaguna = new LatLng(-36.404443, -72.398359);
        mMap.addMarker(new MarkerOptions().position(frentelaguna).title("Zona de Fotografía")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.lugardefotos)));

        LatLng consultorio = new LatLng(-36.397984, -72.399157);
        mMap.addMarker(new MarkerOptions().position(consultorio).title("Centro Médico")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.consultoriopeque)));

        LatLng plaza = new LatLng(-36.393469, -72.397502);
        mMap.addMarker(new MarkerOptions().position(plaza).title("Zona de WIFI y Entretenimiento")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.zonawifipeque)));

        LatLng plazoleta = new LatLng(-36.390468, -72.398305);
        mMap.addMarker(new MarkerOptions().position(plazoleta).title("Zona de WIFI y Entretenimiento")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.zonawifipeque)));

        LatLng parquecerro = new LatLng(-36.3837763,-72.3946578);
        mMap.addMarker(new MarkerOptions().position(parquecerro).title("Parque Cerro Ninhue")
                .icon(BitmapDescriptorFactory.fromResource(R.raw.downhill)));


        float zoomlevel = 14;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(plaza, zoomlevel));


    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
