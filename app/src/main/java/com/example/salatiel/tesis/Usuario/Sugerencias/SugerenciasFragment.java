package com.example.salatiel.tesis.Usuario.Sugerencias;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SugerenciasFragment extends Fragment {

    private Spinner spnrtipo;
    private final static String[] tipos = { "General", "En revision", "Respondida", "Tema cerrado" };
    private HttpParams params;
    private HttpClient usuario;
    private HttpPost post;
    private HttpResponse response;
    private HttpEntity entity;
    private String responseString, ID_Usuario;
    private Button btn01;
    private List<NameValuePair> lista;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sugerencias, container, false);

        listView = (ListView) view.findViewById(R.id.lvsugerencias01);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipos);

        spnrtipo = (Spinner) view.findViewById(R.id.spnrsugerencias01);
        spnrtipo.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fabsugerencias01);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor, new NuevaSugerenciaFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btn01 = (Button) view.findViewById(R.id.btnsugerencias01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 10){
                    StrictMode.ThreadPolicy policy = StrictMode.ThreadPolicy.LAX;
                    StrictMode.setThreadPolicy(policy);
                }
                try{
                    final ArrayList sugerencias = new ArrayList();

                    params = new BasicHttpParams();

                    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

                    usuario = new DefaultHttpClient(params);
                    post = new HttpPost("http://181.75.202.177/WebService/MostrarSugerencias.php");

                    InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    Cursor fila = bd.rawQuery("select * from usuario", null);

                    if (fila.moveToFirst()) {
                        ID_Usuario = String.valueOf(fila.getString(2).equals("1"));
                    }

                    lista = new ArrayList<>();
                    lista.add(new BasicNameValuePair("id_usuario", ID_Usuario));

                    post.setEntity(new UrlEncodedFormEntity(lista, "UTF-8"));

                    response = usuario.execute(post);
                    entity = response.getEntity();

                    responseString = EntityUtils.toString(entity);
                    JSONObject object = new JSONObject(responseString);

                    JSONArray responseJSON = object.getJSONArray("sugerencia");
                    Log.w("Array: ", String.valueOf(responseJSON));

                    for (int i=0; i<responseJSON.length(); i++){
                        JSONObject o = responseJSON.getJSONObject(i);

                        int id_sugerencia = Integer.valueOf(o.getString("id_sugerencia"));
                        String nombre_sugerencia = String.valueOf(o.getString("nombre_sugerencia"));
                        int estado_sugerencia = Integer.valueOf(o.getString("estado_sugerencia"));
                        String detalle_sugerencia = String.valueOf(o.getString("detalle_sugerencia"));
                        int tipo_sugerencia = Integer.valueOf(o.getString("tipo_sugerencia"));

                        sugerencias.add(o.get("nombre_sugerencia"));
                    }

                    final ArrayAdapter<ContentValues> adaptador = new ArrayAdapter<ContentValues>(getContext(), android.R.layout.simple_list_item_1, sugerencias);
                    listView.setAdapter(adaptador);

                }catch (IOException ex){
                    ex.printStackTrace();
                }catch (JSONException ex){
                    ex.printStackTrace();
                    Log.d("entra aqui ", "problema");
                }
            }
        });
        return view;
    }
}
