package com.example.salatiel.tesis.Administrador;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.salatiel.tesis.R;

public class CrearMarcadorAdminFragment extends Fragment {

    private Spinner spnrsector;
    private EditText txt01, txt02;
    private Button btn01, btn02;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crear_marcador_admin, container, false);

        spnrsector = (Spinner) view.findViewById(R.id.spnrcrearmarcador01);

        txt01 = (EditText) view.findViewById(R.id.etcrear01);
        txt02 = (EditText) view.findViewById(R.id.etcrear02);

        btn01 = (Button) view.findViewById(R.id.btncrearmarcador01);
        btn02 = (Button) view.findViewById(R.id.btncrearmarcador02);

        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent01 = new Intent(getActivity(), MarcadorActivity.class);
                startActivityForResult(intent01, 0);
            }
        });

        return view;
    }
}
