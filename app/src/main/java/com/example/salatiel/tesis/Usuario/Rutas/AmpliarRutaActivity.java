package com.example.salatiel.tesis.Usuario.Rutas;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salatiel.tesis.Alarma;
import com.example.salatiel.tesis.AlertaPeligro;
import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class AmpliarRutaActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView mensaje1, mensaje2;
    Double latitud = null;
    Double longitud = null;
    Double MiLatitud = null;
    Double MiLongitud = null;
    Double PLatitud = null;
    Double PLongitud = null;
    Double SLatitud = null;
    Double SLongitud = null;
    private MarkerOptions viejoMarcador = null;
    private Marker MarcadorInicio;
    private Marker ultimoMarcador;
    private ArrayList<ContentValues> puntos = new ArrayList<ContentValues>();
    private ArrayList<ContentValues> peligro = new ArrayList<ContentValues>();
    private ArrayList<ContentValues> seguro = new ArrayList<ContentValues>();
    private Location NuestraUbicacion, UbicacionObjetivo;
    private CircleOptions CirculoDesvio, CirculoPeligro, CirculoSeguro, CirculoInicio, CirculoFinal;
    private float Distancia;
    private Button btn01, btn02;
    Handler mHandler;
    Timer timer = new Timer();
    int EstadoRuta = 1;
    Long tInicio, tFinal, tDiferencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ampliar_ruta);

        mensaje1 = (TextView) findViewById(R.id.tvmain01);
        mensaje2 = (TextView) findViewById(R.id.tvmain02);

        btn01 = (Button) findViewById(R.id.btnampliarruta01);
        btn02 = (Button) findViewById(R.id.btnampliarruta02);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabampliarruta01);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(getApplication(), AlertaPeligro.class));
                if (EstadoRuta == 1){
                    EstadoRuta = 0;
                    Toast.makeText(getApplicationContext(), "el estado es " +EstadoRuta, Toast.LENGTH_SHORT).show();
                } else if (EstadoRuta == 0){
                    EstadoRuta = 1;
                    Toast.makeText(getApplicationContext(), "el estado es " +EstadoRuta, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        UiSettings btnZoom = mMap.getUiSettings();
        btnZoom.setZoomControlsEnabled(true);

        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from coordenadasruta where id_ruta='"+id_ruta+"'", null);
        if (fila.moveToFirst()) {
            if (fila.getString(4).equals("1")){
                latitud = Double.parseDouble(fila.getString(1));
                longitud = Double.parseDouble(fila.getString(2));

                LatLng inicio = new LatLng(latitud, longitud);

                MarcadorInicio = mMap.addMarker(new MarkerOptions().position(inicio).title("inicio").snippet("lat: " + inicio));
                float zoomlevel = 20;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(inicio, zoomlevel));
            }
            do {

                latitud = Double.parseDouble(fila.getString(1));
                longitud = Double.parseDouble(fila.getString(2));

                LatLng casa = new LatLng(latitud, longitud);


                if(puntos.size() >= 1){
                    if(ultimoMarcador != null){
                        ultimoMarcador.remove();
                    }
                    ultimoMarcador = mMap.addMarker(new MarkerOptions().position(casa).title("final").snippet("lat: " + casa));
                } else {
                    mMap.addMarker(new MarkerOptions().position(casa).title("").snippet("lat: " + casa));
                }

                MarkerOptions nuevoMarcador =  new MarkerOptions().anchor(0.0f, 0.7f)
                        .position(casa)
                        .draggable(false);

                if (viejoMarcador == null){
                    viejoMarcador = nuevoMarcador;



                }else {
                    mMap.addPolyline(new PolylineOptions().add(//traza la linea desde el viejomarcador hasta el nuevo marcador

                            viejoMarcador.getPosition(),
                            nuevoMarcador.getPosition()
                            )
                                    .width(3) //ancho
                                    .color(Color.BLACK)  //color
                    );
                    viejoMarcador = nuevoMarcador;
                }
/*
                CirculoGeneral = new CircleOptions()
                        .center(casa)
                        .radius(5)
                        .strokeWidth(3)
                        .strokeColor(Color.GREEN);
                mMap.addCircle(CirculoGeneral);
*/
                ContentValues punto = new ContentValues();
                punto.put("id", puntos.size() + 1);
                punto.put("lat", nuevoMarcador.getPosition().latitude);
                punto.put("lng", nuevoMarcador.getPosition().longitude);

                puntos.add(punto);

            } while(fila.moveToNext());
        }
        bd.close();

        CoordenadasPeligro();
        CoordenadasSeguras();
    }

//inicia la consulta de mi posicion con las coordenadas correctas de la ruta

    public void IniciarRegistro(View view) {
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                IniciarRuta();
                DetectaPeligro();
            }
        };

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Message message = mHandler.obtainMessage();
                message.sendToTarget();
            }
        }, 15*1000, 15*1000);
        btn02.setEnabled(true);
        btn01.setEnabled(false);
    }

    public void DetenerRegistro (View view){
        stopService(new Intent(this, Alarma.class));
        timer.cancel();
        timer = new Timer();
        btn01.setEnabled(true);
        btn02.setEnabled(false);
        RegistrarRecord();
    }

    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setAmpliarRutaActivity(this);
        final  boolean gpsEnable = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnable) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        mensaje1.setText("Latitud");
        mensaje2.setText("Longitud");
    }

    public class Localizacion implements LocationListener {
        AmpliarRutaActivity ampliarRutaActivity;

        public AmpliarRutaActivity getAmpliarRutaActivity() {
            return ampliarRutaActivity;
        }

        public void setAmpliarRutaActivity(AmpliarRutaActivity ampliarRutaActivity) {
            this.ampliarRutaActivity = ampliarRutaActivity;
        }

        @Override
        public void onLocationChanged(Location location) {
            location.getLatitude();
            location.getLongitude();

            String Text1 = ""+location.getLatitude();
            mensaje1.setText(Text1);

            String Text2 = ""+location.getLongitude();
            mensaje2.setText(Text2);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
            mensaje1.setText("GPS Activado");
        }

        @Override
        public void onProviderDisabled(String provider) {
            mensaje1.setText("GPS Desactivado");
        }
    }

    private void IniciarRuta(){
        MiLatitud = Double.parseDouble(mensaje1.getText().toString());
        MiLongitud = Double.parseDouble(mensaje2.getText().toString());

        LatLng MiUbicacion = new LatLng(MiLatitud, MiLongitud);

        NuestraUbicacion = new Location("Punto A");
        NuestraUbicacion.setLatitude(MiUbicacion.latitude);
        NuestraUbicacion.setLongitude(MiUbicacion.longitude);

        tInicio = System.currentTimeMillis();

        for (int i =0; i <puntos.size(); i++){
            Double eslatitud = Double.parseDouble(puntos.get(i).get("lat").toString());
            Double eslongitud = Double.parseDouble(puntos.get(i).get("lng").toString());

            LatLng casa = new LatLng(eslatitud, eslongitud);
            CirculoDesvio = new CircleOptions()
                    .center(casa)
                    .radius(7)
                    .strokeWidth(3)
                    .strokeColor(Color.GREEN);
            mMap.addCircle(CirculoDesvio);

            UbicacionObjetivo = new Location("Punto B");
            UbicacionObjetivo.setLatitude(CirculoDesvio.getCenter().latitude);
            UbicacionObjetivo.setLongitude(CirculoDesvio.getCenter().longitude);

            Distancia = NuestraUbicacion.distanceTo(UbicacionObjetivo);

            if (Distancia < CirculoDesvio.getRadius()){
                stopService(new Intent(this, Alarma.class));
                Toast.makeText(getApplicationContext(), "Distancia hacia el objetivo" +Distancia, Toast.LENGTH_SHORT).show();
                break;
            }
        }
        if (Distancia > CirculoDesvio.getRadius()){
            startService(new Intent(this, Alarma.class));
            Toast.makeText(getApplicationContext(), "Distancia hacia el objetivo" +Distancia, Toast.LENGTH_SHORT).show();
        }
    }

    private void CoordenadasPeligro(){
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from coordenadasruta where id_ruta='"+id_ruta+"' and estado_coordenada = '1'", null);
        if (fila.moveToFirst()) {
            do{
                PLatitud = Double.parseDouble(fila.getString(1));
                PLongitud = Double.parseDouble(fila.getString(2));

                ContentValues coordenadas = new ContentValues();
                coordenadas.put("lat", PLatitud);
                coordenadas.put("lng", PLongitud);

                peligro.add(coordenadas);
            }while(fila.moveToNext());

        }
        bd.close();
    }

    private void CoordenadasSeguras(){
        ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
        int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

        InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from coordenadasruta where id_ruta='"+id_ruta+"' and estado_coordenada = '0'", null);
        if (fila.moveToFirst()) {
            do{
                SLatitud = Double.parseDouble(fila.getString(1));
                SLongitud = Double.parseDouble(fila.getString(2));

                ContentValues coordenadas = new ContentValues();
                coordenadas.put("lat", SLatitud);
                coordenadas.put("lng", SLongitud);

                seguro.add(coordenadas);
            }while(fila.moveToNext());

        }
        bd.close();
    }

    private void DetectaPeligro(){
        MiLatitud = Double.parseDouble(mensaje1.getText().toString());
        MiLongitud = Double.parseDouble(mensaje2.getText().toString());

        LatLng MiUbicacion = new LatLng(MiLatitud, MiLongitud);

        NuestraUbicacion = new Location("Punto A");
        NuestraUbicacion.setLatitude(MiUbicacion.latitude);
        NuestraUbicacion.setLongitude(MiUbicacion.longitude);

        if (EstadoRuta == 1){
            for (int i =0; i <peligro.size(); i++){
                Double eslatitud = Double.parseDouble(peligro.get(i).get("lat").toString());
                Double eslongitud = Double.parseDouble(peligro.get(i).get("lng").toString());

                LatLng casa = new LatLng(eslatitud, eslongitud);
                CirculoPeligro = new CircleOptions()
                        .center(casa)
                        .radius(7)
                        .strokeWidth(3)
                        .strokeColor(Color.GREEN);
                mMap.addCircle(CirculoPeligro);

                UbicacionObjetivo = new Location("Punto B");
                UbicacionObjetivo.setLatitude(CirculoPeligro.getCenter().latitude);
                UbicacionObjetivo.setLongitude(CirculoPeligro.getCenter().longitude);

                Distancia = NuestraUbicacion.distanceTo(UbicacionObjetivo);

                if (Distancia < CirculoPeligro.getRadius()){
                    startService(new Intent(this, AlertaPeligro.class));
                    break;
                }
            }
            if (CirculoPeligro != null){
                if (Distancia > CirculoPeligro.getRadius()){
                    stopService(new Intent(this, AlertaPeligro.class));

                }
            }
        } else if (EstadoRuta == 0){
            for (int i =0; i <seguro.size(); i++){
                Double eslatitud = Double.parseDouble(seguro.get(i).get("lat").toString());
                Double eslongitud = Double.parseDouble(seguro.get(i).get("lng").toString());

                LatLng casa = new LatLng(eslatitud, eslongitud);
                CirculoSeguro = new CircleOptions()
                        .center(casa)
                        .radius(7)
                        .strokeWidth(3)
                        .strokeColor(Color.GREEN);
                mMap.addCircle(CirculoSeguro);

                UbicacionObjetivo = new Location("Punto B");
                UbicacionObjetivo.setLatitude(CirculoSeguro.getCenter().latitude);
                UbicacionObjetivo.setLongitude(CirculoSeguro.getCenter().longitude);

                Distancia = NuestraUbicacion.distanceTo(UbicacionObjetivo);

                if (Distancia < CirculoSeguro.getRadius()){
                    startService(new Intent(this, AlertaPeligro.class));
                    EstadoRuta = 1;
                    break;
                }
            }
            if (CirculoSeguro != null){
                if (Distancia > CirculoSeguro.getRadius()){
                    stopService(new Intent(this, AlertaPeligro.class));
                }
            }
        }
    }
    private void RegistrarRecord(){
        tFinal = System.currentTimeMillis();
        tDiferencia = tFinal - tInicio;
        Double SegundosTranscurridos = tDiferencia / 1000.0;
        SegundosTranscurridos = SegundosTranscurridos /60;
        Toast.makeText(getApplicationContext(), "Tu Tiempo del recorrido es: " +SegundosTranscurridos, Toast.LENGTH_SHORT).show();

        InicioSQLiteOpenHelper adminu = new InicioSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase bd = adminu. getWritableDatabase();

        Cursor fila = bd.rawQuery("select * from usuario", null);
        if (fila.moveToFirst()) {
            int id_usuario = Integer.parseInt(fila.getString(2));
            ArrayList<ContentValues> laruta = (ArrayList<ContentValues>) getIntent().getSerializableExtra("LaRuta");
            int id_ruta = Integer.parseInt(laruta.get(0).get("id_ruta").toString());

            ContentValues registror = new ContentValues();
            registror.put("id_ruta", id_ruta);
            registror.put("id_usuario", id_usuario);
            registror.put("tiempo", SegundosTranscurridos);

            bd.insert("record", null, registror);
        }
        bd.close();
    }
}
