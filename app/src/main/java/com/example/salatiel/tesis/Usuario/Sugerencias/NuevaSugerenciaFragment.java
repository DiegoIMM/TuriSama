package com.example.salatiel.tesis.Usuario.Sugerencias;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.salatiel.tesis.DB.InicioSQLiteOpenHelper;
import com.example.salatiel.tesis.R;
import com.example.salatiel.tesis.Usuario.RegistroActivity;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class NuevaSugerenciaFragment extends Fragment {

    private Spinner spnrtipo;
    private final static String[] tipos = { "Tipo sugerencia", "Editar ruta", "Deshabilitar ruta", "Sugerencia a la aplicacion" };
    private Button btn01, btn02;
    private EditText txt01, txt02;
    private HttpClient usuario;
    private HttpPost post;
    private List<NameValuePair> lista;
    private String token;
    int tipo_sugerencia;

    private boolean datos(){
        if (spnrtipo.getSelectedItem().equals("Tipo sugerencia")){
            tipo_sugerencia = 0;
        } else if (spnrtipo.getSelectedItem().equals("Editar ruta")){
            tipo_sugerencia = 1;
        } else if (spnrtipo.getSelectedItem().equals("Deshabilitar ruta")){
            tipo_sugerencia = 2;
        } else if (spnrtipo.getSelectedItem().equals("Sugerencia a la aplicacion")){
            tipo_sugerencia = 3;
        }
        if (tipo_sugerencia == 0){
            Toast.makeText(getContext(), "Ingrese un tipo de sugerencia", Toast.LENGTH_SHORT).show();
        }else{
            usuario = new DefaultHttpClient();
            post = new HttpPost("http://181.75.202.177/WebService/InsertarSugerencia.php");
            lista = new ArrayList<NameValuePair>(5);

            InicioSQLiteOpenHelper admin = new InicioSQLiteOpenHelper(getContext(), "administracion", null, 1);
            SQLiteDatabase bd = admin.getWritableDatabase();

            Cursor fila = bd.rawQuery("select * from usuario", null);

            if (fila.moveToFirst()) {
                token = fila.getString(0);
            }

            lista.add(new BasicNameValuePair("token", token));
            lista.add(new BasicNameValuePair("nombre", txt01.getText().toString().trim()));
            lista.add(new BasicNameValuePair("estado", "1"));
            lista.add(new BasicNameValuePair("detalle", txt02.getText().toString().trim()));
            lista.add(new BasicNameValuePair("tipo", String.valueOf(tipo_sugerencia)));

            Log.d("prueba: ", String.valueOf(lista));

            try{
                post.setEntity(new UrlEncodedFormEntity(lista));
                usuario.execute(post);
                return true;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_nueva_sugerencia, container, false);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, tipos);

        spnrtipo = (Spinner) view.findViewById(R.id.spnrnuevasugerencia01);
        spnrtipo.setAdapter(adapter);

        btn01 = (Button) view.findViewById(R.id.btnnuevasugerencia01);
        btn02 = (Button) view.findViewById(R.id.btnnuevasugerencia02);

        txt01 = (EditText) view.findViewById(R.id.etnuevasugerencia01);
        txt02 = (EditText) view.findViewById(R.id.etnuevasugerencia02);

        class EnviarDatos extends AsyncTask<String, String, String> {
            private Activity contexto;

            EnviarDatos(Fragment context){
                this.contexto = getActivity();
            }

            @Override
            protected String doInBackground(String... params) {
                if (datos()){
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos enviados exitosamente", Toast.LENGTH_SHORT).show();
                            txt01.setText("");
                            txt02.setText("");
                        }
                    });
                }else {
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos no enviados", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return null;
            }

        }

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txt01.getText()) ||
                        TextUtils.isEmpty(txt02.getText())){
                    Toast.makeText(getContext(), "Hay campos sin llenar", Toast.LENGTH_SHORT).show();
                }else {
                    new EnviarDatos(NuevaSugerenciaFragment.this).execute();

                }
            }
        });

        return view;
    }

}
