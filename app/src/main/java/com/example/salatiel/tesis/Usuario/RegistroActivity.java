package com.example.salatiel.tesis.Usuario;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.salatiel.tesis.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class RegistroActivity extends AppCompatActivity {

    private EditText txt01, txt02, txt03, txt04, txt05, txt06, txt07;
    private HttpClient usuario;
    private HttpPost post;
    private List<NameValuePair> lista;
    private Button btn01, btn02;

    private boolean datos(){
        usuario = new DefaultHttpClient();
        post = new HttpPost("http://181.75.202.177/WebService/InsertarUsuario.php");
        lista = new ArrayList<NameValuePair>(5);

        lista.add(new BasicNameValuePair("nombre", txt01.getText().toString().trim()));
        lista.add(new BasicNameValuePair("fecha", txt02.getText().toString().trim()));
        lista.add(new BasicNameValuePair("telefono", txt03.getText().toString().trim()));
        lista.add(new BasicNameValuePair("email", txt04.getText().toString().trim()));
        lista.add(new BasicNameValuePair("contrasena", txt06.getText().toString().trim()));

        try{
            post.setEntity(new UrlEncodedFormEntity(lista));
            usuario.execute(post);
            return true;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txt01 = (EditText) findViewById(R.id.etregistro01);
        txt02 = (EditText) findViewById(R.id.etregistro02);
        txt03 = (EditText) findViewById(R.id.etregistro03);
        txt04 = (EditText) findViewById(R.id.etregistro04);
        txt05 = (EditText) findViewById(R.id.etregistro05);
        txt06 = (EditText) findViewById(R.id.etregistro06);
        txt07 = (EditText) findViewById(R.id.etregistro07);

        btn01 = (Button)findViewById(R.id.btnregistro01);
        btn02 = (Button)findViewById(R.id.btnregistro02);

        class EnviarDatos extends AsyncTask<String, String, String> {
            private Activity contexto;
            EnviarDatos(Activity context){
                this.contexto = context;
            }
            @Override
            protected String doInBackground(String... params) {
                if (datos()){
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos enviados exitosamente", Toast.LENGTH_SHORT).show();
                            txt01.setText("");
                            txt02.setText("");
                            txt03.setText("");
                            txt04.setText("");
                            txt05.setText("");
                            txt06.setText("");
                            txt07.setText("");
                        }
                    });
                }else {
                    contexto.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(contexto, "Datos no enviados", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return null;
            }
        }

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txt01.getText()) ||
                        TextUtils.isEmpty(txt02.getText())){
                    Toast.makeText(getApplicationContext(), "Hay campos sin llenar", Toast.LENGTH_SHORT).show();
                }else{
                    if (txt04.getText().toString().equals(txt05.getText().toString())){
                        if (txt06.getText().toString().equals(txt07.getText().toString())){
                            new EnviarDatos(RegistroActivity.this).execute();
                        }else{
                            Toast.makeText(getApplicationContext(), "Contraseña no coincide", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Email no coincide", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt01.setText("");
                txt02.setText("");
                txt03.setText("");
                txt04.setText("");
                txt05.setText("");
                txt06.setText("");
                txt07.setText("");

                Toast.makeText(getApplicationContext(), "Campos vacios", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
